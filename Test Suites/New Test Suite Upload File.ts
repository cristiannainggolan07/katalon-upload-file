<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>New Test Suite Upload File</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>10111662-6e68-4719-9c55-4e3e22bb7635</testSuiteGuid>
   <testCaseLink>
      <guid>1e91f6af-645b-41b0-a16d-c665e8062e55</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Data Driven Test Case Upload</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>605154db-067c-4f34-b035-d11dfbd450a3</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/New Test Data Upload</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>605154db-067c-4f34-b035-d11dfbd450a3</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Path</value>
         <variableId>7973a150-f202-48fd-9d4d-3c2e60a86ada</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
